//
//  SizeExtensions.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import Foundation

extension CGSize {
    mutating func scale(by amount: Double) {
        self.width *= amount
        self.height *= amount
    }
}
