//
//  ResizableModifier.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import Foundation
import SwiftUI

struct Resizable : ViewModifier {
    
    let fontSize: Int
    let minimumScaleFactor: CGFloat
    
    func body(content: Content) -> some View {
        content
            .font(.system(size: 125))
            .minimumScaleFactor(minimumScaleFactor)
    }
}

extension View {
    func resizable(fontSize: Int = 125, minimumScaleFactor: CGFloat = 0.01) -> some View {
        let modifier = Resizable(fontSize: fontSize, minimumScaleFactor: minimumScaleFactor)
        return self.modifier(modifier)
    }
}
