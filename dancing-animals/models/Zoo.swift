//
//  Zoo.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import Foundation

struct Zoo {
    var animals: [Animal] = [
        Animal(name: "Cow", emoji: "🐄"),
        Animal(name: "Dog", emoji: "🐕"),
        Animal(name: "Tiger", emoji: "🐅"),
    ]
}
