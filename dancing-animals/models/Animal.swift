//
//  Animal.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import Foundation

struct Animal : Identifiable {
    let id = UUID()
    
    let name: String
    let emoji: String
}
