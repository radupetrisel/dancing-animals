//
//  ContentView.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var zoo: ZooViewModel
    
    var body: some View {
        NavigationStack {
            ScrollView {
                ForEach(zoo.animals) { animal in
                    AnimalRowView(animal: animal)
                        .overlay {
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(.white, lineWidth: 2.0)
                        }
                        .padding(3)
                }
                
                NavigationLink("Dance") {
                    DanceView()
                }
            }
        }
        .frame(width: 600, height: 1000)
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(ZooViewModel(zoo: Zoo()))
    }
}
