//
//  AnimalViewModel.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import Foundation

struct AnimalViewModel : Identifiable {
    let id: UUID
    let name: String
    var emojiViewModel: EmojiViewModel
    var offset = CGSize(width: 0.0, height: 0.0)
    
    init(animal: Animal) {
        self.id = animal.id
        self.name = animal.name
        self.emojiViewModel = EmojiViewModel(emoji: animal.emoji)
    }
}
