//
//  EmojiViewModel.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import Foundation

struct EmojiViewModel {
    let emoji: String
    var scale = CGSize(width: 1.0, height: 1.0)
    var offset = CGSize(width: 0.0, height: 0.0)
    var rotation: Double = 0.0
    
    init(emoji: String, scale: CGSize = CGSize(width: 1.0, height: 1.0), offset: CGSize = CGSize(width: 0.0, height: 0.0), rotation: Double = 0) {
        self.emoji = emoji
        self.scale = scale
        self.offset = offset
        self.rotation = rotation
    }
}
