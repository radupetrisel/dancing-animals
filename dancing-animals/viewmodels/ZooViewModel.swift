//
//  ZooViewModel.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import Foundation

class ZooViewModel : ObservableObject {
    @Published var animals: [AnimalViewModel]
    
    init(zoo: Zoo) {
        self.animals = zoo.animals.map { animal in
            AnimalViewModel(animal: animal)
        }
    }
    
    func dance() {
        for index in animals.indices {
            animals[index].emojiViewModel.offset = CGSize.random(in: 0.0..<200.0)
            animals[index].emojiViewModel.rotation = Double.random(in: 0.0..<360.0)
        }
    }
}

extension CGSize {
    static func random(in range: Range<Double>) -> CGSize {
        return CGSize(width: Double.random(in: range),
                      height: Double.random(in: range))
    }
}
