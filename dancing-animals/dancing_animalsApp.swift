//
//  dancing_animalsApp.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import SwiftUI

@main
struct dancing_animalsApp: App {
    let zoo = ZooViewModel(zoo: Zoo())
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(zoo)
        }
    }
}
