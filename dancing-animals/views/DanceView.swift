//
//  DanceView.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import SwiftUI

struct DanceView: View {
    @EnvironmentObject var zoo: ZooViewModel
    
    var body: some View {
        ZStack {
            ForEach(zoo.animals) { animal in
                Text(animal.emojiViewModel.emoji)
                    .resizable()
                    .offset(animal.emojiViewModel.offset)
                    .rotationEffect(Angle(degrees: animal.emojiViewModel.rotation))
                    .animation(.easeIn, value: animal.emojiViewModel.rotation)
                    .animation(.linear, value: animal.emojiViewModel.offset)
            }
        }
        .onTapGesture {
            zoo.dance()
        }
        .resizable()
    }
}

struct DanceView_Previews: PreviewProvider {
    static var previews: some View {
        DanceView().environmentObject(ZooViewModel(zoo: Zoo()))
            .frame(width: 400, height: 1000)
    }
}
