//
//  AnimalRowView.swift
//  dancing-animals
//
//  Created by Radu Petrisel on 09.02.2023.
//

import SwiftUI

struct AnimalRowView: View {
    let animal: AnimalViewModel
    
    var body: some View {
        HStack {
            Text(animal.name)
                .font(.largeTitle)
                .padding()
            
            Spacer()
            
            Text(animal.emojiViewModel.emoji)
                .resizable()
        }
        .padding()
    }
}

struct AnimalRowView_Previews: PreviewProvider {
    static var previews: some View {
        AnimalRowView(animal: AnimalViewModel(animal: Animal(name: "Dodo", emoji: "🦤")))
    }
}
